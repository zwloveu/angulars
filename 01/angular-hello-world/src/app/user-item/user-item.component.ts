import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {
  // we utilize the ! symbol to indicate that we are aware that 
  // name is not initialized in the constructor 
  // and we will handle it elsewhere.
  @Input() name!: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
